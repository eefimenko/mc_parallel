#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<assert.h>
#include<curand.h>
#include<curand_kernel.h>

#define THREADS 256
#define NDIM 5

double f(double x, double y, double z, double u, double v){
    return exp(-x*x - y*y - z*z -u*u -v*v); 
}

/* this is the GPU kernel function to calculate integral by MC method */
__global__ void run_mc(unsigned int seed, int n, double a, double b, double* int_f) {
    /* this array is shared for all of the threads and contains partial summs*/
    __shared__ double summs[THREADS];

    /* make a random state for each thread*/
    curandState_t state;
    curand_init(seed, threadIdx.x, 0, &state);

    /* calc full summ */
    double summ = 0;
    int n_per_thread = n/THREADS+1;
  
    /* for each iteration */
    for (int j = 0; j < n_per_thread; j++) {
	if(threadIdx.x*n_per_thread + j >= n)
	{
	    break;
	}
	double x = a + curand_uniform_double(&state)*(b - a);
	double y = a + curand_uniform_double(&state)*(b - a);
	double z = a + curand_uniform_double(&state)*(b - a);
	double u = a + curand_uniform_double(&state)*(b - a);
	double v = a + curand_uniform_double(&state)*(b - a);
	summ += exp(-x*x - y*y - z*z -u*u -v*v);
    }

    /* calculate summ for this thread */
    summs[threadIdx.x] = summ;

    /* now we need to wait for all threads to calculate their partial summs */
    __syncthreads( );

    /* now we summate all of the partial summs using the reduction scheme */
    int i = THREADS / 2;
    while (i != 0) {
	/* if we are not thread 0 */
	if (threadIdx.x < i) {
	    /* summing in the one to our right by i places into this one */
	    summs[threadIdx.x] = summs[threadIdx.x] + summs[threadIdx.x + i];
	}

	/* wait for all threads to do this */
	__syncthreads( );
	i /= 2;
    }

    /* now array slot 0 should hold the final answer - have one thread write it to the output cell */
    if (threadIdx.x == 0) {
	*int_f = summs[0]/(n);
    }
}

double uran(double a, double b){
    return rand() / (RAND_MAX + 1.0) * (b - a) + a;
}


double timer(void)
{
    struct timeval time;
    gettimeofday(&time, 0);
    return time.tv_sec + time.tv_usec/1000000.0;
}

int main(int argc, char **argv){
    int n;
    double a, b;
    double vol=1.0, int_f = 0.0;
    double t1,t2;
    
    double* gpu_int_f;
    cudaMalloc((void**) &gpu_int_f, sizeof(double));
	
    a = 0.0; b = 1.0; /* upper and lower bounds of the integral */
    vol = pow((b-a), NDIM);         /* compute n-dimensional volume */
    
    assert(argc == 2);
    n = atoi(argv[1]);
    t1 = timer();

    run_mc<<<1,THREADS>>>(time(NULL), n, a, b, gpu_int_f);
    cudaMemcpy(&int_f, gpu_int_f, sizeof(double), cudaMemcpyDeviceToHost);
    cudaFree(gpu_int_f);
    int_f /= vol;
    t2 = timer();
    double exact= pow(sqrt(M_PI)/2.*erf(1.), 5.);
    printf("GPU MC integral is: %.8lf exact is: %.8lf accuracy is: %le\n", int_f, exact, fabs((exact-int_f)/exact));
    printf("Time is %lf s\n", t2 - t1);
    FILE* out = fopen("cuda_mc.dat", "w");
    if (NULL != out)
    {
	fprintf(out, "%lf\n%le\n", t2 - t1, fabs((exact-int_f)/exact));
	fclose(out);
    }
    return 0;
}


