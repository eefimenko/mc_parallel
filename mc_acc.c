#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<assert.h>
#include <openacc.h>
#include <accelmath.h>

#define NDIM 5

double uran(double a, double b){
    return rand() / (RAND_MAX + 1.0) * (b - a) + a;
}

double timer(void)
{
    struct timeval time;
    gettimeofday(&time, 0);
    return time.tv_sec + time.tv_usec/1000000.0;
}

#pragma acc routine 
double f(double x, double y, double z, double u, double v){
    return exp(-x*x - y*y - z*z -u*u -v*v); 
}

int main(int argc, char **argv){
    int i,n,n0=2000000,res;
    double a, b;
    double vol=1.0, int_f = 0.0, int_f1 = 0.0;
    double t1,t2;
    double x,y,z,u,v;
    double* rand_array;
    
    a = 0.0; b = 1.0; /* upper and lower bounds of the integral */
    vol = pow((b-a), NDIM);         /* compute n-dimensional volume */
    
    assert(argc == 2);
    n = atoi(argv[1]);
    res = n;
    /* precompute random numbers for MC simulations*/
    rand_array = malloc(sizeof(double)*NDIM*n0);
    t1 = timer();
    while (res > 0)
    {
	if (res <= n0)
	{
	    n0 = res;
	    res = 0; 
	}
	for (i=0; i<NDIM*n0; i++)
	{
	    rand_array[i] = uran(a,b);
	}
        
#pragma acc parallel loop private(x,y,z,u,v) reduction(+:int_f)
	for (i = 0; i < n0; ++i){
	    x = rand_array[5*i];
	    y = rand_array[5*i+1];
	    z = rand_array[5*i+2];
	    u = rand_array[5*i+3];
	    v = rand_array[5*i+4];
	    int_f += f(x,y,z,u,v);      /* calc the the summ of function */
	}
	res -= n0;
    }
    

    int_f *= vol/n;           /* scale by n-dimensional volume and number of points */
    t2 = timer();
    double exact = pow(sqrt(M_PI)/2.*erf(1.), 5.);
    
    printf("OpenACC MC integral is: %.8lf exact is: %.8lf accuracy is: %le\n", int_f, exact, fabs((exact-int_f)/exact));
    printf("Time is %lf s\n", t2 - t1);
    free(rand_array);
    FILE* out = fopen("acc_mc.dat", "w");
    if (NULL != out)
    {
	fprintf(out, "%lf\n%le\n", t2 - t1, fabs((exact-int_f)/exact));
	fclose(out);
    }

    return 0;
}


