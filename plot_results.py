#!/usr/bin/python
import matplotlib.pyplot as plt

def read_file(file):
    f = open(file)
    n = []
    r = []
    for line in f:
        tmp = line.split()
        n.append(int(tmp[0]))
        r.append(float(tmp[1]))
    return n,r

def main():
   
    fig = plt.figure(figsize=(20,10))
    ax = fig.add_subplot(2,2,1)
    n1, r1 = read_file('time_omp_mc.dat')
    n2, r2 = read_file('time_cuda_mc.dat')
    n3, r3 = read_file('time_acc_mc.dat')
    
    ax.plot(n1, r1, 'r', label='OpenMP')
    ax.plot(n2, r2, 'g', label='CUDA')
    ax.plot(n3, r3, 'b', label='OpenACC')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylabel('time,s')
    ax.set_xlabel('Number of samples')
    ax.set_title('MC time of execution')
    plt.legend(loc='upper left')
    n1, ra1 = read_file('accuracy_omp_mc.dat')
    n2, ra2 = read_file('accuracy_cuda_mc.dat')
    n3, ra3 = read_file('accuracy_acc_mc.dat')
    ax = fig.add_subplot(2,2,2)
    ax.plot(n1, ra1, 'r', label='OpenMP')
    ax.plot(n2, ra2, 'g', label='CUDA')
    ax.plot(n3, ra3, 'b', label='OpenACC')
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_ylabel('Accuracy')
    ax.set_xlabel('Number of samples')
    ax.set_title('MC accuracy')
    plt.legend(loc='upper right')
    f = open('MC.txt', 'w')
    f.write('%10s\t%s\t%s\t\t%s\t\t%s\t%s\t\t%s\n'%('#samples','T OpenMP','T CUDA','T ACC','A OpenMP','A CUDA','A ACC'))
    for i in range(len(n1)):
        f.write('%10d\t%lf\t%lf\t%lf\t%le\t%le\t%le\n'%(n1[i],r1[i],r2[i],r3[i],ra1[i],ra2[i],ra3[i]))
    f.close()
    n1, r1 = read_file('time_omp_sr.dat')
    n2, r2 = read_file('time_cuda_sr.dat')
    n3, r3 = read_file('time_acc_sr.dat')
    ax = fig.add_subplot(2,2,3)
    ax.plot(n1, r1, 'r', label='OpenMP')
    ax.plot(n2, r2, 'g', label='CUDA')
    ax.plot(n3, r3, 'b', label='OpenACC')
    ax.set_ylabel('time,s')
    ax.set_yscale('log')
    ax.set_xlabel('Number of points')
    plt.legend(loc='upper left')
    ax.set_title('SR time of execution')
    n1, ra1 = read_file('accuracy_omp_sr.dat')
    n2, ra2 = read_file('accuracy_cuda_sr.dat')
    n3, ra3 = read_file('accuracy_acc_sr.dat')
    ax = fig.add_subplot(2,2,4)
    ax.plot(n1, ra1, 'r', label='OpenMP')
    ax.plot(n2, ra2, 'g', label='CUDA')
    ax.plot(n3, ra3, 'b', label='OpenACC')
    ax.set_yscale('log')
    ax.set_ylabel('Accuracy')
    ax.set_xlabel('Number of points')
    plt.legend(loc='upper right')
    ax.set_title('SR accuracy')
    f = open('SR.txt', 'w')
    f.write('%3s\t%s\t%s\t\t%s\t\t%s\t%s\t\t%s\n'%('#points','T OpenMP','T CUDA','T ACC','A OpenMP','A CUDA','A ACC'))
    for i in range(len(n1)):
        f.write('%3d\t%lf\t%lf\t%lf\t%le\t%le\t%le\n'%(n1[i],r1[i],r2[i],r3[i],ra1[i],ra2[i],ra3[i]))
    f.close()
    plt.tight_layout()
    plt.savefig('result.png')
    plt.close()

if __name__ == "__main__":
    main()
