mc_min=1000
mc_max=20000000
mc_step=2
mc=$mc_min

sr_min=3
sr_max=16
sr_step=1
sr=$sr_min
rm "time_omp_mc.dat" >& /dev/null
rm "accuracy_omp_mc.dat" >& /dev/null
rm "time_cuda_mc.dat" >& /dev/null
rm "accuracy_cuda_mc.dat" >& /dev/null
rm "time_acc_mc.dat" >& /dev/null
rm "accuracy_acc_mc.dat" >& /dev/null
rm "time_omp_sr.dat" >& /dev/null
rm "accuracy_omp_sr.dat" >& /dev/null
rm "time_cuda_sr.dat" >& /dev/null
rm "accuracy_cuda_sr.dat" >& /dev/null
rm "time_acc_sr.dat" >& /dev/null
rm "accuracy_acc_sr.dat" >& /dev/null
while [ $mc -le $mc_max ]
do
    ./mc_omp $mc
    ./mc $mc
    ./mc_acc $mc
    mc=$[$mc*$mc_step]
    time=`head -1 omp_mc.dat | tail -1 `
    accuracy=`head -2 omp_mc.dat | tail -1`
    echo $mc $time >> "time_omp_mc.dat"
    echo $mc $accuracy >> "accuracy_omp_mc.dat"
    time=`head -1 cuda_mc.dat | tail -1 `
    accuracy=`head -2 cuda_mc.dat | tail -1`
    echo $mc $time >> "time_cuda_mc.dat"
    echo $mc $accuracy >> "accuracy_cuda_mc.dat"
    time=`head -1 acc_mc.dat | tail -1 `
    accuracy=`head -2 acc_mc.dat | tail -1`
    echo $mc $time >> "time_acc_mc.dat"
    echo $mc $accuracy >> "accuracy_acc_mc.dat"
done

while [ $sr -le $sr_max ]
do
    ./simpson_omp $sr
    ./simpson $sr
    ./simpson_acc $sr
    sr=$[$sr+$sr_step]
    time=`head -1 omp_sr.dat | tail -1 `
    accuracy=`head -2 omp_sr.dat | tail -1`
    echo $sr $time >> "time_omp_sr.dat"
    echo $sr $accuracy >> "accuracy_omp_sr.dat"
    time=`head -1 cuda_sr.dat | tail -1 `
    accuracy=`head -2 cuda_sr.dat | tail -1`
    echo $sr $time >> "time_cuda_sr.dat"
    echo $sr $accuracy >> "accuracy_cuda_sr.dat"
    time=`head -1 acc_sr.dat | tail -1 `
    accuracy=`head -2 acc_sr.dat | tail -1`
    echo $sr $time >> "time_acc_sr.dat"
    echo $sr $accuracy >> "accuracy_acc_sr.dat"
done
