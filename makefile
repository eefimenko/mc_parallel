# User options

ACC = pgcc
CC = gcc
NVCC = nvcc
OPTIMIZE = yes
DEBUG    = no
OPENMP   = yes

# Program and source code

PROGRAM1 = mc_omp
PROGRAM2 = simpson_omp
PROGRAM3 = mc
PROGRAM4 = simpson
PROGRAM5 = mc_acc
PROGRAM6 = simpson_acc

HEADERS = 

SOURCE1 = mc_omp.c
OBJECTS1 = $(SOURCE1:.c=.o)
SOURCE2 = simpson_omp.c
OBJECTS2 = $(SOURCE2:.c=.o)
SOURCE3 = mc.cu
OBJECTS3 = $(SOURCE3:.cu=.o)
SOURCE4 = simpson.cu
OBJECTS4 = $(SOURCE4:.cu=.o)
SOURCE5 = mc_acc.c
OBJECTS5 = $(SOURCE5:.c=.o)
SOURCE6 = simpson_acc.c
OBJECTS6 = $(SOURCE6:.c=.o)

# Set flags
CUFLAGS = -O2 -lcurand
CFLAGS = -Wall
LDFLAGS = -lm
ACCFLAGS = -Minfo=accel -acc 

ifeq ($(DEBUG),yes)
  CFLAGS += -g3
  LDFLAGS  += -g
endif

ifeq ($(OPTIMIZE),yes)
  CFLAGS += -O3
endif

CFLAGS += -fopenmp

# Targets to build
all: $(PROGRAM1) $(PROGRAM2) $(PROGRAM3) $(PROGRAM4) $(PROGRAM5) $(PROGRAM6)

$(PROGRAM1): $(OBJECTS1) $(HEADERS)
	$(CC) $(CFLAGS) $(OBJECTS1) -o $@ $(LDFLAGS)

$(PROGRAM2): $(OBJECTS2) $(HEADERS)
	$(CC) $(CFLAGS) $(OBJECTS2) -o $@ $(LDFLAGS)

$(PROGRAM3): $(OBJECTS3) $(HEADERS)
	$(NVCC) $(OBJECTS3) -o $@ $(LDFLAGS)

$(PROGRAM4): $(OBJECTS4) $(HEADERS)
	$(NVCC) $(CUFLAGS) $(OBJECTS4) -o $@ $(LDFLAGS)

$(PROGRAM5): $(SOURCE5) $(HEADERS)
	$(ACC) $(ACCFLAGS) $(SOURCE5) -o $@ $(LDFLAGS)

$(PROGRAM6): $(SOURCE6) $(HEADERS)
	$(ACC) $(ACCFLAGS) $(SOURCE6) -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.cu
	$(NVCC) $(CUFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS1) $(PROGRAM1) $(OBJECTS2) $(PROGRAM2) $(OBJECTS5) $(PROGRAM5) $(OBJECTS6) $(PROGRAM6) $(OBJECTS3) $(PROGRAM3) $(OBJECTS4) $(PROGRAM4) *~ *.dat

run: $(PROGRAM1) $(PROGRAM2) $(PROGRAM3) $(PROGRAM4) $(PROGRAM5) $(PROGRAM6)
	bash run_test.sh

openmp: $(PROGRAM1) $(PROGRAM2) 

cuda: $(PROGRAM3) $(PROGRAM4)

acc: $(PROGRAM5) $(PROGRAM6)

plot: run
	python plot_results.py

help:
	@echo "make - build all binaries"
	@echo "make openmp - build OpenMP binaries"
	@echo "make cuda - build CUDA binaries"
	@echo "make acc - build OpenACC binaries"
	@echo "make run - run tests"
	@echo "make clean - remove all binaries, object, temporary files"
	@echo "make plot - plot figure with results"
	@echo "make help - print help"
