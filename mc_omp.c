#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<assert.h>
#include <omp.h>

#define NDIM 5

double uran(double a, double b, unsigned int* seed);

double f(double x, double y, double z, double u, double v){
    return exp(-x*x -y*y - z*z  - u*u - v*v); 
}

double timer(void)
{
    struct timeval time;
    gettimeofday(&time, 0);
    return time.tv_sec + time.tv_usec/1000000.0;
}

int main(int argc, char **argv){
    int i,n;
    double a, b;
    double vol=1.0, int_f = 0.0;
    double t1,t2;
    double x,y,z,u,v;
    unsigned int seed;
    
    a = 0.0; b = 1.0; /* upper and lower bounds of the integral */
    vol = pow((b-a), NDIM);         /* compute n-dimensional volume */
    
    assert(argc == 2);
    n = atoi(argv[1]);
    /* precompute random numbers for MC simulations*/
    
    t1 = timer();
#pragma omp parallel private(seed)
    {
	seed = 25234 + 17*omp_get_thread_num();
#pragma omp for private(x,y,z,u,v,seed) schedule(guided) reduction(+:int_f)    
	for (i = 0; i < n; ++i){
	    x = uran(a,b,&seed);
	    y = uran(a,b,&seed);
	    z = uran(a,b,&seed);
	    u = uran(a,b,&seed);
	    v = uran(a,b,&seed);
	    int_f += f(x,y,z,u,v);      /* calc the the summ of function */
	}
    }   

    int_f *= vol/n;           /* scale by n-dimensional volume and number of points */
    t2 = timer();
    double exact = pow(sqrt(M_PI)/2.*erf(1.), 5.);
    printf("OpenMP MC integral is: %.8lf exact is: %.8lf accuracy is: %le\n", int_f, exact, fabs((exact-int_f)/exact));
    printf("Time is %lf s\n", t2 - t1);
    FILE* out = fopen("omp_mc.dat", "w");
    if (NULL != out)
    {
	fprintf(out, "%lf\n%le\n", t2 - t1, fabs((exact-int_f)/exact));
	fclose(out);
    }
    return 0;
}

double uran(double a, double b, unsigned int* seed){
    return rand_r(seed) / (RAND_MAX + 1.0) * (b - a) + a;
}

