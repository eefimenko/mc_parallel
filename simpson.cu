#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<assert.h>
#include<curand.h>
#include<curand_kernel.h>

#define THREADS 256
#define NDIM 5

double f(double x, double y, double z, double u, double v){
    return exp(-x*x - y*y - z*z -u*u -v*v); 
}

/* this is the GPU kernel function to calculate integral by MC method */
__global__ void run_simpson(int n, int nmax, double a, double delta, double* int_f) {
    /* this array is shared for all of the threads and contains partial summs*/
    __shared__ double summs[THREADS];

    /* calc full summ */
    double summ = 0., x,y,z,u,v;
    double h = delta/2.;
    double w[3];
  
    int n_per_thread = nmax/THREADS+1;
    w[0] = h/3.; w[1] = 4.*w[0]; w[2] = w[0];
    /* for each iteration */
    for (int i0 = 0; i0 < n_per_thread; i0++) {
	int res = threadIdx.x*n_per_thread + i0;
	if(res >= nmax)
	{
	    break;
	}
	int i = res%n;
	res = res/n; 
	int ii = res%3;
	res = res/3;
	int j = res%n;
	res = res/n; 
	int jj = res%3;
	res = res/3;
	int k = res%n;
	res = res/n; 
	int kk = res%3;
	res = res/3;
	int l = res%n;
	res = res/n; 
	int ll = res%3;
	res = res/3;
	int m = res%n;
	res = res/n; 
	int mm = res%3;

	x = a + i * delta + ii * h;
	y = a + j * delta + jj * h;
	z = a + k * delta + kk * h;
	u = a + l * delta + ll * h;
	v = a + m * delta + mm * h;
	summ += w[ii]*w[jj]*w[kk]*w[ll]*w[mm]*exp(-x*x-y*y-z*z-u*u-v*v);
      
    }

    /* calculate summ for this thread */
    summs[threadIdx.x] = summ;

    /* now we need to wait for all threads to calculate their partial summs */
    __syncthreads( );

    /* now we summate all of the partial summs using the reduction scheme */
    int i = THREADS / 2;
    while (i != 0) {
	/* if we are not thread 0 */
	if (threadIdx.x < i) {
	    /* summing in the one to our right by i places into this one */
	    summs[threadIdx.x] = summs[threadIdx.x] + summs[threadIdx.x + i];
	}

	/* wait for all threads to do this */
	__syncthreads( );
	i /= 2;
    }

    /* now array slot 0 should hold the final answer - have one thread write it to the output cell */
    if (threadIdx.x == 0) {
	*int_f = summs[0];
    }
}

double timer(void)
{
    struct timeval time;
    gettimeofday(&time, 0);
    return time.tv_sec + time.tv_usec/1000000.0;
}

int main(int argc, char **argv){
    int n;
    double a, b;
    double delta, h, int_f = 0.0;
    double t1,t2;
   
    double w[3];
    
    double* gpu_int_f;
    cudaMalloc((void**) &gpu_int_f, sizeof(double));
	
    a = 0.0; b = 1.0; /* upper and lower bounds of the integral */
    
    assert(argc == 2);
    n = atoi(argv[1]);
    delta =(b-a)/n;	   /* "grid spacing" -- fixed interval between function sample points */
    h = delta / 2.0;
    w[0] = h/3.; w[1] = 4.*w[0]; w[2] = w[0];
    t1 = timer();
    int nmax = pow(3*n, NDIM);

    run_simpson<<<1,THREADS>>>(n, nmax, a, delta, gpu_int_f);
    cudaMemcpy(&int_f, gpu_int_f, sizeof(double), cudaMemcpyDeviceToHost);
    cudaFree(gpu_int_f);
    
    t2 = timer();
    double exact= pow(sqrt(M_PI)/2.*erf(1.), 5.);
    printf("GPU Simpson's rule integral is: %.8lf exact is: %.8lf accuracy is: %le\n", int_f, exact, fabs((exact-int_f)/exact));
    printf("Time is %lf s\n", t2 - t1);
    FILE* out = fopen("cuda_sr.dat", "w");
    if (NULL != out)
    {
	fprintf(out, "%lf\n%le\n", t2 - t1, fabs((exact-int_f)/exact));
	fclose(out);
    }
    return 0;
}


