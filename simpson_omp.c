#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<time.h>
#include<sys/time.h>

double timer(void)
{
    struct timeval time;
    gettimeofday(&time, 0);
    return time.tv_sec + time.tv_usec/1000000.0;
}

double f(double x, double y, double z, double u, double v){
    return exp(-x*x -y*y - z*z  - u*u - v*v); 
}

int main(int argc, char **argv)
{
    // inputs
    int i,ii,j,jj,k,kk,l,ll,m,mm,n;
    double a, b;
    double delta, h, integral = 0.0;
    double x,y,z,u,v;
    double w[3];
    double t1,t2;
  
    assert(argc == 2);
    n = atoi(argv[1]); /* "resolution", ie number of points at which to sample f */
    a = 0.; b = 1.; /* upper and lower bounds of the integral */
    delta =(b-a)/n;   /* "grid spacing" -- fixed interval between function sample points */
    h = delta / 2.0;  /* h is used for convenience to find half distance between adjacent samples */
    integral = 0.0;   /* the accumulated integral estimate */

    /* three point weights that define Simpson's rule */
    w[0] = h/3.; w[1] = 4.*w[0]; w[2] = w[0];
    t1 = timer();
#pragma omp parallel for private(x,y,z,u,v,j,jj,i,ii,k,kk,l,ll,m,mm) shared(h,delta) reduction(+:integral)
    for (j = 0; j < n; j++){
	for (jj=0;jj<3;++jj){
	    y = a + j * delta + jj * h;
	    for( i = 0; i < n; i++ ){
		for (ii=0;ii<3;++ii){
		    x = a + i * delta + ii * h;
		    for (k = 0; k < n; k++){
			for (kk=0;kk<3;++kk){
			    z = a + k * delta + kk * h;
			    for( l = 0; l < n; l++ ){
				for (ll=0;ll<3;++ll){
				    u = a + l * delta + ll * h;
				    for( m = 0; m < n; m++ ){
					for (mm=0;mm<3;++mm){
					    v = a + m * delta + mm * h;
					    integral += w[ii]*w[jj]*w[kk]*w[ll]*w[mm]*f(x,y,z,u,v);
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    t2 = timer();
    double exact = pow(sqrt(M_PI)/2.*erf(1.), 5.);
    printf("OpenMP Simpson's rule integral is: %.8lf exact is: %.8lf accuracy is: %le\n", integral, exact, fabs((exact-integral)/exact));
    printf("Time is %lf s\n", t2 - t1);
    FILE* out = fopen("omp_sr.dat", "w");
    if (NULL != out)
    {
	fprintf(out, "%lf\n%le\n", t2 - t1, fabs((exact-integral)/exact));
	fclose(out);
    }
    return 0;
}
